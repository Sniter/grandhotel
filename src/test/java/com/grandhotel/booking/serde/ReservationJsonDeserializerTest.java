package com.grandhotel.booking.serde;

import com.grandhotel.booking.model.Reservation;
import com.grandhotel.booking.model.Room;
import com.grandhotel.booking.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@JsonTest
@Import(SerdeConfiguration.class)
class ReservationJsonDeserializerTest {

    @MockBean
    RoomRepository roomRepository;

    @Autowired
    private JacksonTester<Reservation> jsonTester;

    private String reservation, invalidReservation;
    private Room room;
    private Calendar start, end;

    @BeforeEach
    void setUp(){
        start = new GregorianCalendar(2021,Calendar.DECEMBER, 1);
        end = new GregorianCalendar(2021,Calendar.DECEMBER, 31);
        reservation = "{\"start\":\"2021-12-01\",\"end\":\"2021-12-31\",\"room\":1}";
        invalidReservation = "{\"start\":\"2021-12-01\",\"end\":\"2021-12-31\"}";
        room = Room.builder().withNumber(1).withFloor(2).withRooms(3).build();
    }

    @Test
    public void checkReservationDeserialization() throws IOException {
        when(roomRepository.getById(1)).thenReturn(room);

        Reservation reservation = jsonTester.parseObject(this.reservation);
        assertThat(reservation.getStart()).isEqualTo(start);
        assertThat(reservation.getEnd()).isEqualTo(end);
        assertThat(reservation.getRoom()).isEqualTo(room);
        assertThat(reservation.getId()).isNull();
    }

    @Test
    public void checkInvalidReservationDeserialization() throws IOException {
        when(roomRepository.getById(1)).thenReturn(room);

        Reservation reservation = jsonTester.parseObject(this.invalidReservation);
        assertThat(reservation.getStart()).isEqualTo(start);
        assertThat(reservation.getEnd()).isEqualTo(end);
        assertThat(reservation.getRoom()).isNull();
        assertThat(reservation.getId()).isNull();
    }



}