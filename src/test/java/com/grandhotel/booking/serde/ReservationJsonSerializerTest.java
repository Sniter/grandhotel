package com.grandhotel.booking.serde;

import com.grandhotel.booking.model.Reservation;
import com.grandhotel.booking.model.Room;
import com.grandhotel.booking.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@JsonTest
@Import(SerdeConfiguration.class)
class ReservationJsonSerializerTest {

    @MockBean
    RoomRepository roomRepository;

    @Autowired
    private JacksonTester<Reservation> jsonTester;

    private Room room;
    private Reservation reservation, reservationWithoutRoom;
    private Calendar start, end;

    @BeforeEach
    void setUp(){
        start = new GregorianCalendar(2021,Calendar.DECEMBER, 1);
        end = new GregorianCalendar(2021,Calendar.DECEMBER, 31);
        room = Room.builder().withNumber(1).withFloor(2).withRooms(3).build();
        reservation = Reservation.builder()
                .withStart(start)
                .withEnd(end)
                .withRoom(room)
                .build();
        reservationWithoutRoom = Reservation.builder()
                .withStart(start)
                .withEnd(end)
                .build();
    }

    @Test
    public void checkReservationSerialization() throws IOException {
        JsonContent<Reservation> json = jsonTester.write(reservation);
        assertThat(json).extractingJsonPathStringValue("$.start").isEqualTo("2021-12-01");
        assertThat(json).extractingJsonPathStringValue("$.end").isEqualTo("2021-12-31");
        assertThat(json).extractingJsonPathNumberValue("$.room").isEqualTo(1);
    }

    @Test
    public void checkReservationWithoutRoomSerialization() throws IOException {
        JsonContent<Reservation> json = jsonTester.write(reservationWithoutRoom);
        assertThat(json).extractingJsonPathStringValue("$.start").isEqualTo("2021-12-01");
        assertThat(json).extractingJsonPathStringValue("$.end").isEqualTo("2021-12-31");
        assertThat(json).extractingJsonPathNumberValue("$.room").isNull();
    }

}