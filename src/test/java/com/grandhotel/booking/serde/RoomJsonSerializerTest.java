package com.grandhotel.booking.serde;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.grandhotel.booking.model.Room;
import com.grandhotel.booking.repository.RoomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@JsonTest
@Import(SerdeConfiguration.class)
class RoomJsonSerializerTest {

    @MockBean
    RoomRepository roomRepository;

    @Autowired
    private JacksonTester<Room> jsonTester;

    private Room room, roomWithoutRooms, roomWithoutFloor, roomWithOnlyNumber, emptyRoom;

    @BeforeEach
    void setUp(){
        room = Room.builder().withNumber(1).withFloor(2).withRooms(3).build();
        roomWithoutRooms = Room.builder().withNumber(1).withFloor(2).build();
        roomWithoutFloor = Room.builder().withNumber(1).withRooms(3).build();
        roomWithOnlyNumber = Room.builder().withNumber(1).build();
        emptyRoom = Room.builder().build();
    }

    @Test
    public void checkRoomSerialization() throws IOException {
        JsonContent<Room> response = jsonTester.write(room);
        assertThat(response).extractingJsonPathNumberValue("$.number").isEqualTo(1);
        assertThat(response).extractingJsonPathNumberValue("$.floor").isEqualTo(2);
        assertThat(response).extractingJsonPathNumberValue("$.rooms").isEqualTo(3);
    }

    @Test
    public void checkRoomWithoutRoomsSerializationFailrure() throws IOException {

        Assertions.assertThrows(JsonMappingException.class, () -> {
            JsonContent<Room> response = jsonTester.write(roomWithoutRooms);
        });
    }

    @Test
    public void checkRoomWithoutFloorSerializationFailure() throws IOException {
        Assertions.assertThrows(JsonMappingException.class, () -> {
            JsonContent<Room> response = jsonTester.write(roomWithoutFloor);
        });
    }

    @Test
    public void checkRoomWithNumberOnlySerializationFailure() throws IOException {
        Assertions.assertThrows(JsonMappingException.class, () -> {
            JsonContent<Room> response = jsonTester.write(roomWithOnlyNumber);
        });
    }

    @Test
    public void checkEmptyRoomSerializationFailure() throws IOException {
        Assertions.assertThrows(JsonMappingException.class, () -> {
            JsonContent<Room> response = jsonTester.write(emptyRoom);
        });
    }


}