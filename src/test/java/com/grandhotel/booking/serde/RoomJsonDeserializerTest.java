package com.grandhotel.booking.serde;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.grandhotel.booking.model.Room;
import com.grandhotel.booking.repository.RoomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@JsonTest
@Import(SerdeConfiguration.class)
class RoomJsonDeserializerTest {

    @MockBean
    RoomRepository roomRepository;

    @Autowired
    private JacksonTester<Room> jsonTester;

    private String room, invalidRoom;

    @BeforeEach
    void setUp(){
        room = "{\"number\":1,\"floor\":2,\"rooms\":3}";
        invalidRoom = "{\"number\":\"f00\",\"floor\":\"2021\",\"rooms\":\"10\"}";
    }

    @Test
    public void checkRoomParsing() throws IOException {
        Room parsedRoom = jsonTester.parseObject(room);
        assertThat(parsedRoom.getNumber()).isEqualTo(1);
        assertThat(parsedRoom.getFloor()).isEqualTo(2);
        assertThat(parsedRoom.getRooms()).isEqualTo(3);
    }

    @Test
    public void checkInvalidRoomParsing() throws IOException {
        Assertions.assertThrows(ClassCastException.class, () -> {
            jsonTester.parseObject(invalidRoom);
        });
    }



}