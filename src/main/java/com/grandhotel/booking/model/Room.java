package com.grandhotel.booking.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.grandhotel.booking.serde.RoomJsonDeserializer;
import com.grandhotel.booking.serde.RoomJsonSerializer;
import lombok.*;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@JsonSerialize(using = RoomJsonSerializer.class)
@JsonDeserialize(using = RoomJsonDeserializer.class)
@Entity
@Table(name="rooms")
@Builder(setterPrefix="with")
@Getter @Setter @ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor @AllArgsConstructor
public class Room implements Serializable {
    @Id
    private Integer number;
    private Integer rooms;
    private Integer floor;

//    @OneToMany(mappedBy = "room")
//    private Set<Reservation> reservations = new HashSet<>();

    @ReadOnlyProperty
    @Version
    private Long version;
}
