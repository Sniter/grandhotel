package com.grandhotel.booking.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.grandhotel.booking.serde.ReservationJsonDeserializer;
import com.grandhotel.booking.serde.ReservationJsonSerializer;
import lombok.*;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@JsonSerialize(using = ReservationJsonSerializer.class)
@JsonDeserialize(using = ReservationJsonDeserializer.class)
@Entity
@Table(name="reservations")
@Builder(setterPrefix="with")
@Getter @Setter @ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor @AllArgsConstructor
public class Reservation implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Calendar start;

    @Temporal(TemporalType.DATE)
    @Column(name = "end_date")
    private Calendar end;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room")
    private Room room;

    @ReadOnlyProperty
    @Version
    private Long version;
}
