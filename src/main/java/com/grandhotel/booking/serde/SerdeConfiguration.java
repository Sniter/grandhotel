package com.grandhotel.booking.serde;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
public class SerdeConfiguration {
    @Bean
    public DateFormat calendarFormat(){
        return new SimpleDateFormat("yyyy-MM-dd");
    }
}
