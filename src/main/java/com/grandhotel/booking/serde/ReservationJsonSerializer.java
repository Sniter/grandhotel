package com.grandhotel.booking.serde;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.grandhotel.booking.model.Reservation;
import com.grandhotel.booking.model.Room;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@JsonComponent
@RequiredArgsConstructor
public class ReservationJsonSerializer extends JsonSerializer<Reservation> {
    private final DateFormat calendarFormat;

    @Override
    public void serialize(Reservation value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        if(value.getId() != null)
            gen.writeNumberField("id", value.getId());
        else
            gen.writeNullField("id");
        gen.writeStringField("start", calendarFormat.format(value.getStart().getTime()));
        gen.writeStringField("end", calendarFormat.format(value.getEnd().getTime()));
        if(value.getRoom() != null)
            gen.writeNumberField("room", value.getRoom().getNumber());
        else
            gen.writeNullField("room");
        gen.writeEndObject();
    }
}
