package com.grandhotel.booking.serde;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.grandhotel.booking.model.Room;

import java.io.IOException;

public class RoomJsonDeserializer extends JsonDeserializer<Room> {
    @Override
    public Room deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        final TreeNode treeNode = p.getCodec().readTree(p);
        final NumericNode roomNumber = (NumericNode) treeNode.get("number");
        final NumericNode floor = (NumericNode) treeNode.get("floor");
        final NumericNode rooms = (NumericNode) treeNode.get("rooms");
        return Room.builder()
                .withNumber(roomNumber.asInt())
                .withFloor(floor.asInt())
                .withRooms(rooms.asInt())
                .build();
    }
}
