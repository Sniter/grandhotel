package com.grandhotel.booking.serde;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.grandhotel.booking.model.Reservation;
import com.grandhotel.booking.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

@JsonComponent
@RequiredArgsConstructor
public class ReservationJsonDeserializer extends JsonDeserializer<Reservation> {
    private final RoomRepository roomRepository;
    private final DateFormat calendarFormat;

    private Calendar fromString(String value) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(calendarFormat.parse(value));
        return cal;
    }

    @SneakyThrows
    @Override
    public Reservation deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        final TreeNode treeNode = p.getCodec().readTree(p);
        final TextNode startDate = (TextNode) treeNode.get("start");
        final TextNode endDate = (TextNode) treeNode.get("end");
        final NumericNode roomNumber = (NumericNode) treeNode.get("room");

        Reservation.ReservationBuilder builder = Reservation.builder();

        builder = builder
                .withStart(fromString(startDate.asText()))
                .withEnd(fromString(endDate.asText()));

        if (roomNumber != null && !roomNumber.isMissingNode())
            builder = builder.withRoom(roomRepository.getById(roomNumber.asInt()));

        return builder.build();
    }
}
