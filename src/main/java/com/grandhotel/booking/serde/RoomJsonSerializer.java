package com.grandhotel.booking.serde;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.grandhotel.booking.model.Room;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class RoomJsonSerializer extends JsonSerializer<Room> {
    @Override
    public void serialize(Room value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("number", value.getNumber());
        gen.writeNumberField("rooms", value.getRooms());
        gen.writeNumberField("floor", value.getFloor());
        gen.writeEndObject();
    }

}
